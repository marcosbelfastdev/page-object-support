import io.github.bonigarcia.wdm.WebDriverManager;
import org.base.erbium.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class AdvancedOptionsTests {

    EDriver browser;
    private WebDriver driver;
    private BaseState baseState;

    @BeforeTest
    public void setUp() throws Exception {

        // Core Selenium initialisations
        WebDriverManager.chromedriver().driverVersion("2.40")
                .setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-notifications");
        options.addArguments("--incognito");
        driver = new ChromeDriver();

        // Erbium base state definitions
        // Output directory will be: /home/<user>/Erbium Tests/...
        BaseState baseState = new BaseState();
        baseState.setProjectName("Page Object Tests");
        baseState.setEnvironment("DEV");
        baseState.setTestName(getClass().getSimpleName());
        baseState.setBaseStateUrl("https://demoqa.com/automation-practice-form/");

        // Erbium driver initialisations
        browser = baseState.execute(driver);

    }

    @Test
    public void testSharedSettings() throws Exception {
        browser.setOption(AdvancedOptions.PAGE_OBJECT_OPTIONS, PageObjectsOptions.SHARED);
        browser.setOption(AdvancedOptions.PAGE_LOAD_TIMEOUT, 123456);
        System.out.println("Expected: 123456 - Obtained: " + browser.getOption(AdvancedOptions.PAGE_LOAD_TIMEOUT));
        MyPageObject myPageObject = new MyPageObject(browser);
        myPageObject.changePageTimeoutTo(200000);
        System.out.println("Expected: 200000 - Obtained: " + browser.getOption(AdvancedOptions.PAGE_LOAD_TIMEOUT));
    }

    @Test
    public void testForkResetSettings() {
        browser.setOption(AdvancedOptions.PAGE_OBJECT_OPTIONS, PageObjectsOptions.FORK_RESET);
        browser.setOption(AdvancedOptions.PAGE_LOAD_TIMEOUT, 123456);
        System.out.println("Expected: 123456 - Obtained: " + browser.getOption(AdvancedOptions.PAGE_LOAD_TIMEOUT));
        MyPageObject myPageObject = new MyPageObject(browser);
        myPageObject.printPageLoadTimeout(); // expected default different than 123456
        System.out.println("Expected: 123456 - Obtained: " + browser.getOption(AdvancedOptions.PAGE_LOAD_TIMEOUT));

    }

    @Test
    public void testForkCopySettings() {
        browser.setOption(AdvancedOptions.PAGE_OBJECT_OPTIONS, PageObjectsOptions.FORK_COPY);
        browser.setOption(AdvancedOptions.PAGE_LOAD_TIMEOUT, 123456);
        System.out.println("Expected: 123456 - Obtained: " + browser.getOption(AdvancedOptions.PAGE_LOAD_TIMEOUT));
        MyPageObject myPageObject = new MyPageObject(browser);
        myPageObject.printPageLoadTimeout(); // expected 123456
        myPageObject.changePageTimeoutTo(333333);
        myPageObject.printPageLoadTimeout();
        System.out.println("Expected: 123456 - Obtained: " + browser.getOption(AdvancedOptions.PAGE_LOAD_TIMEOUT));
    }

    @AfterTest
    public void tearDown() throws Exception {
        try {
            browser.quit();
        } catch (Exception ignore) {

        }
    }

}


