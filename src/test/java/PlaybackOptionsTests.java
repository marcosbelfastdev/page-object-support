
import org.base.erbium.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;


public class PlaybackOptionsTests {

    EDriver browser;
    private WebDriver driver;
    private BaseState baseState;

    @BeforeTest
    public void setUp() throws Exception {

        // Core Selenium initialisations
        WebDriverManager.chromedriver().driverVersion("2.40")
                .setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-notifications");
        options.addArguments("--incognito");
        driver = new ChromeDriver();

        // Erbium base state definitions
        // Output directory will be: /home/<user>/Erbium Tests/...
        BaseState baseState = new BaseState();
        baseState.setProjectName("Page Object Tests");
        baseState.setEnvironment("DEV");
        baseState.setTestName(getClass().getSimpleName());
        baseState.setBaseStateUrl("https://demoqa.com/automation-practice-form/");

        // Erbium driver initialisations
        browser = baseState.execute(driver);
        browser.manage().window().maximize();
        browser.setOption(Common.INTERACT_DELAY_AFTER, 1500);

        browser.manage().highlightElements(true);
        // or browser.setOption(Common.HIGHLIGHT_ELEMENTS);

    }

    @Test
    public void testSharedSettings() throws Exception {
        browser.setOption(AdvancedOptions.PAGE_OBJECT_OPTIONS, PageObjectsOptions.SHARED);
        // default highlight is green
        browser.setOption(Common.HIGHLIGHT_STYLE, "border: 2px solid red; border-radius: 5px;");
        browser.find(By.id("firstName")).setText("Red"); // should be red
        // Create a page object
        MyPageObject myPageObject = new MyPageObject(browser);
        myPageObject.fillLastName("Red Again"); // should be red
        myPageObject.setColourPurple();        // should be purple
        browser.find(By.id("userEmail")).setText("Purple expected.");
    }

    @Test
    public void testForkResetSettings() {
        browser.setOption(AdvancedOptions.PAGE_OBJECT_OPTIONS, PageObjectsOptions.FORK_RESET);
        // default highlight style is green
        browser.setOption(Common.HIGHLIGHT_STYLE, "border: 2px solid purple; border-radius: 5px;");
        MyPageObject myPageObject = new MyPageObject(browser);
        myPageObject.fillUserNumber("Green expected."); // should reset to light green
        browser.find(By.id("currentAddress")).setText("Main browser 1. Purple expected.");
    }

    @Test
    public void testForkCopySettings() {
        browser.setOption(AdvancedOptions.PAGE_OBJECT_OPTIONS, PageObjectsOptions.FORK_COPY);
        // default highlight style is green
        browser.setOption(Common.HIGHLIGHT_STYLE, "border: 2px solid purple; border-radius: 5px;");
        MyPageObject myPageObject = new MyPageObject(browser);
        myPageObject.fillUserNumber("Purple expected."); // should reset to light green
        myPageObject.setColourOrange();
        myPageObject.fillLastName("Orange expected.");
        browser.find(By.id("currentAddress")).setText("Purple expected.");
    }

    @AfterTest
    public void tearDown() throws Exception {
        try {
            Thread.sleep(5000);
            browser.quit();
        } catch (Exception ignore) {

        }
    }

}


