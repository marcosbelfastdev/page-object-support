import org.base.erbium.*;
import org.openqa.selenium.By;

public class MyPageObject extends ErbiumPageObject {

    EDriver browser;

    public MyPageObject(EDriver edriver) {
        super(edriver);
        this.browser = super.getDriver();
        browser.setOption(Common.HIGHLIGHT_ELEMENTS);
    }

    public void fillLastName(String text) {
        browser.find(By.id("lastName")).setText(text);
    }

    public void fillUserNumber(String text) {
        EElement element = browser.find(By.id("userNumber"));
        element.setText(text);
    }

    public void setColourPurple() {
        System.out.println("setColourPurple");
        browser.setOption(Common.HIGHLIGHT_STYLE, "border: 2px solid purple; border-radius: 5px;");
        System.out.println("END setColourPurple");
    }

    public void setColourOrange() {
        browser.setOption(Common.HIGHLIGHT_STYLE, "border: 2px solid orange; border-radius: 5px;");
    }

    public void changePageTimeoutTo(int timeout) {
        browser.setOption(AdvancedOptions.PAGE_LOAD_TIMEOUT, timeout);
    }

    public void printPageLoadTimeout() {
        System.out.println(browser.getOption(AdvancedOptions.PAGE_LOAD_TIMEOUT));
    }
}
